import React, { useReducer } from 'react'
import initialvalue from './Store/innitial'
import reducer from './Store/reducer'
import storeState from './Store/store'

function App({ children }) {

  const [state, dispatch] = useReducer(reducer, initialvalue)
  return (
    <storeState.Provider value={[state, dispatch]}>
      {children}
    </storeState.Provider>
  )
}

export default App