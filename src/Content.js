import React from 'react'
import useStore from './Store/context'
import * as action from './Store/action'



export default function Content() {
    const [state, dispatch] = useStore()
    return (
        <>
            <p>Count: {state}</p>
            <button onClick={() => dispatch(action.actionUp(1))}>actionUp</button>
            <button onClick={() => dispatch(action.actionDown(1))}>actionDown</button>
        </>
    )
}
