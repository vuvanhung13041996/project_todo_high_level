import { useContext } from "react";
import storeState from "./store";

const useStore = () => {
    const [state, dispatch] = useContext(storeState)
    return [state, dispatch]
}



export default useStore