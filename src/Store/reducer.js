import { UP, DOWN } from "./contstan"

const reducer = (state, action) => {
    let newState
    switch (action.type) {
        case UP:
            newState = state + action.payload
            break;
        case DOWN:
            newState = state - action.payload
            break;
        default:
            newState = state
    }


    return newState
}

export default reducer