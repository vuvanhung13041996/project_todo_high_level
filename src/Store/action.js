import { UP, DOWN } from "./contstan"


const actionUp = (payload) => {
    return {
        type: UP,
        payload: payload
    }
}


const actionDown = (payload) => {
    return {
        type: DOWN,
        payload: payload
    }
}


export { actionDown, actionUp }